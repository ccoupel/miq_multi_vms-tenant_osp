#
# Description: provide the dynamic list content from available networks
#
module ManageIQ
  module Automate
    module Cloud
      module Orchestration
        module Operations
          class AvailableFlavors
            def initialize(handle = $evm)
              @handle = handle
            end

            def main
              fill_dialog_field(fetch_list_data)
            end

            private

            def fetch_list_data
              service = @handle.root.attributes["service_template"] || @handle.root.attributes["service"]
               tenant=$evm.root["dialog_tenant_name"]
              networks = service.try(:orchestration_manager).try(:security_group)
              networks=$evm.vmdb(:ems_openstack).all.first.miq_templates

              network_list = {}
#              networks.each { |f| network_list[f.ems_ref] = f.name if ((f.cloud_tenant.name rescue "")==tenant && f.name!="default")
              networks.each { |f| network_list[f.ems_ref] = f.name #if ((f.cloud_tenant.name rescue "")==tenant)
                $evm.log(:info, " network=#{f.name}@#{f.cloud_tenant.name rescue ""}")
              } if networks

              return nil => "<none>" if network_list.blank?

              network_list[nil] = "<select>" if network_list.length > 1
              network_list
            end

            def fill_dialog_field(list)
              dialog_field = @handle.object

              # sort_by: value / description / none
              dialog_field["sort_by"] = "description"

              # sort_order: ascending / descending
              dialog_field["sort_order"] = "ascending"

              # data_type: string / integer
              dialog_field["data_type"] = "string"

              # required: true / false
              dialog_field["required"] = "true"

              dialog_field["values"] = list
              dialog_field["default_value"] = nil
            end
          end
        end
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  ManageIQ::Automate::Cloud::Orchestration::Operations::AvailableFlavors.new.main
end
