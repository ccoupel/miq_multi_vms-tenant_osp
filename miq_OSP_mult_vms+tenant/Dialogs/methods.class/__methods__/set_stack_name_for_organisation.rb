#
# Description: <Method description here>
#

begin
  name=$evm.root["dialog_param_tenant_name"]
  id=$evm.root["dialog_param_tenant_id"]
  env=$evm.root["dialog_param_environment"]
  
  $evm.object["value"]="#{name}-#{env}-#{id}_#{rand(36**10).to_s(36)}"
  $evm.object["values"]={$evm.object["value"] => $evm.object["value"]}
  $evm.object["default_value"]=$evm.object["value"]
  $evm.object["visible"]=true
  $evm.object["read_only"]=($evm.object["values"].count <= 1)
end
