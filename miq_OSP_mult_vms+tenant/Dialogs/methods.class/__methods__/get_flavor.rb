#
# Description: provide the dynamic list content from available networks
#
module ManageIQ
  module Automate
    module Cloud
      module Orchestration
        module Operations
          class AvailableFlavors
            def initialize(handle = $evm)
              @handle = handle
            end

            def main
              fill_dialog_field(fetch_list_data)
            end

            private

            def fetch_list_data
              service = @handle.root.attributes["service_template"] || @handle.root.attributes["service"]
              networks = service.try(:orchestration_manager).try(:keypair)
              networks = $evm.vmdb(:ems_openstack).all.first.flavors
              
              network_list = {}
              networks.each { |f| network_list[f.ems_ref] = "#{f.name}: #{f.cpus} cpus, #{f.memory/1024/1024} Mb, #{f.root_disk_size/1024/1024/1024} Gb root disk" } if networks

              return nil => "<none>" if network_list.blank?

              network_list[nil] = "<select>" if network_list.length > 1
              network_list
            end

            def fill_dialog_field(list)
              dialog_field = @handle.object

              # sort_by: value / description / none
              dialog_field["sort_by"] = "description"

              # sort_order: ascending / descending
              dialog_field["sort_order"] = "ascending"

              # data_type: string / integer
              dialog_field["data_type"] = "string"

              # required: true / false
              dialog_field["required"] = "true"

              dialog_field["values"] = list
              dialog_field["default_value"] = nil
            end
          end
        end
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  ManageIQ::Automate::Cloud::Orchestration::Operations::AvailableFlavors.new.main
end
