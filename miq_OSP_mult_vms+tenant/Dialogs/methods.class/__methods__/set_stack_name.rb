#
# Description: <Method description here>
#

begin
  value=$evm.root["dialog_param_vm_prefix"]
  tenant=$evm.root["dialog_tenant_name"]
  $evm.object["value"]="#{tenant}-#{value}_#{rand(36**10).to_s(36)}"
  #$evm.object["value"]="stack-#{value}"
  $evm.object["values"]={$evm.object["value"] => $evm.object["value"]}
  $evm.object["default_value"]=$evm.object["value"]
  $evm.object["visible"]=true
  $evm.object["read_only"]=($evm.object["values"].count <= 1)
end
