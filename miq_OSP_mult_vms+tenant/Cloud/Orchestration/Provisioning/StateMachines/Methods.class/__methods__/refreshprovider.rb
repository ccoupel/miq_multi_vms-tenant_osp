#
# Description: This class checks to see if the stack has been provisioned
#   and whether the refresh has completed
#
module ManageIQ
  module Automate
    module Cloud
      module Orchestration
        module Provisioning
          module StateMachines
            class RefreshProvider
              def initialize(handle = $evm)
                @handle = handle
              end

              def main
                task = @handle.root["service_template_provision_task"]
                service = task.try(:destination)

                unless service
                  @handle.log(:error, 'Service is nil')
                  raise 'Service is nil'
                end
				refresh_provider(service)
              end

              private

              def refresh_provider(service)
                provider = service.orchestration_manager
                stack    = service.orchestration_stack

                @handle.log("info", "Refreshing provider '#{provider.name}'")
                @handle.vmdb(:orchestration_stack).refresh(provider.id,stack.id)
                provider.refresh
                service.name=service.get_dialog_option("dialog_stack_name") unless service.get_dialog_option("dialog_stack_name").blank?
              end              
            end
          end
        end
      end
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  ManageIQ::Automate::Cloud::Orchestration::Provisioning::StateMachines::RefreshProvider.new.main
end
